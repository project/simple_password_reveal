/**
 * @file
 * Toggles password field between password/text input field.
 */

(function simplePasswordReveal() {
  const vanillaCheckbox = document.getElementById(
    'edit-simple-password-reveal-checkbox',
  );
  const vanillaPasswordFields = document.querySelectorAll(
    '#edit-pass, #edit-current-pass, #edit-pass-pass1, #edit-pass-pass2',
  );

  function togglePasswordFields() {
    [].forEach.call(
      vanillaPasswordFields,
      function toggleFieldType(vanillaPasswordField) {
        if (vanillaCheckbox.checked) {
          vanillaPasswordField.type = 'password';
        } else {
          vanillaPasswordField.type = 'text';
        }
      },
    );
  }

  if (vanillaCheckbox !== null) {
    togglePasswordFields();

    vanillaCheckbox.addEventListener('change', function handleCheckboxChange() {
      togglePasswordFields();
    });
  }
})();
